$(function () {
	$('[data-toggle="tooltip"]').tooltip();

	$('[data-toggle="popover"]').popover();

	$('.carousel').carousel({
		interval:2000
	});

	$('#modalContacto').on('show.bs.modal', function(e){
		console.log('el modal se está mostrando');
		$('#btnContacto').removeClass('btn-outline-success');
		$('#btnContacto').addClass('btn-primary');
		$('#btnContacto').prop('disabled', true);
	});
	$('#modalContacto').on('shown.bs.modal', function(e){
		console.log('el modal se mostró');
	});
	$('#modalContacto').on('hide.bs.modal', function(e){
		console.log('el modal se está ocultando');
	});
	$('#modalContacto').on('hidden.bs.modal', function(e){
		console.log('el modal se ocultó');
	$('#btnContacto').removeClass('btn-primary');
		$('#btnContacto').addClass('btn-outline-success');
		$('#btnContacto').prop('disabled', false);
	});
});